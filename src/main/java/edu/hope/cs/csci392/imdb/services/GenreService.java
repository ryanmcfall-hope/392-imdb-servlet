package edu.hope.cs.csci392.imdb.services;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.hope.cs.csci392.imdb.Database;

@Path("genres")
public class GenreService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)	
	public Response findAllGenres() {
		try {
			Database db = Database.getInstance();
			List<String> genres = db.findGenres();
			return Response.ok(genres).build();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.serverError().entity("An exception occurred communicating with the database").build();
		}
	}
}
