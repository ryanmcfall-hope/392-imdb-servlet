package edu.hope.cs.csci392.imdb.services;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.hope.cs.csci392.imdb.Database;
import edu.hope.cs.csci392.imdb.model.Actor;
import edu.hope.cs.csci392.imdb.model.Role;

@Path("actors")
@Produces(MediaType.APPLICATION_JSON)
public class ActorService {

	private static final long serialVersionUID = -4129386223171713871L;

	private static class SearchRequest {
		public String firstName;
		public String lastName;
		public int birthYear;
		public int deathYear;
	}
		
	@GET
	@Path("{id}")
	public Response findActorByID(@PathParam("id") String personID) {
		try {
			Database db = Database.getInstance();
			Actor actor = db.findActorByID(personID);
			return Response.ok(actor).build();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.serverError().entity("An exception occurred communicating with the database").build();
		}
	}
	
	@GET
	@Path("{id}/roles")
	public Response findRolesForActor(@PathParam("id") String personID) {
		try {
			Database db = Database.getInstance();
			List<Role> roles = db.findMoviesForActor(db.findActorByID(personID));
			return Response.ok(roles).build();
		} catch (SQLException e) {
			e.printStackTrace();
			return Response.serverError().entity("An exception occurred communicating with the database").build();
		}
	}
	
	@POST
	@Path("search")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchByActor(SearchRequest request) {	
		try {
			Database db = Database.getInstance();
			List<Actor> actors = db.findActors(request.firstName, request.lastName, request.birthYear, request.deathYear);
			return Response.ok(actors).build();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.serverError().entity("An exception occurred communicating with the database").build();
		}
	}
	
}
