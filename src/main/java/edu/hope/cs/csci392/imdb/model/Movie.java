package edu.hope.cs.csci392.imdb.model;

public class Movie {
	private String titleID;
	private String title;	
	private int yearReleased;
	private int runningTimeInMinutes;
	private String primaryGenre;
	
	public Movie (
		String titleID, String title, int yearReleased, 
		int lengthInMinutes, String primaryGenre
	) {
		this.titleID = titleID;
		this.title = title;
		this.yearReleased = yearReleased;
		this.runningTimeInMinutes = lengthInMinutes;
		this.primaryGenre = primaryGenre;
	}

	public String getMovieID() {
		return titleID;
	}

	public String getTitle() {
		return title;
	}

	public int getYearReleased() {
		return yearReleased;
	}

	public int getLengthInMinutes() {
		return runningTimeInMinutes;
	}

	public String getPrimaryGenre() {
		return primaryGenre;
	}
}