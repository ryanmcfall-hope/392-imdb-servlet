package edu.hope.cs.csci392.imdb;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.hope.cs.csci392.imdb.model.Actor;
import edu.hope.cs.csci392.imdb.model.Movie;
import edu.hope.cs.csci392.imdb.model.Role;

public class Database {
	private static Database instance;
	
	Actor hanks = new Actor (
		"nm0000158" , "Tom", null, "Hanks", null, "Tom Hanks", 1956, 0
	);
	
	Actor ryan = new Actor (
		"nm0000212", "Meg", null, "Ryan", null, "Meg Ryan", 1961, 0
	);
	
	Movie sleepless = new Movie (
		"tt0108160", "Sleepless in Seattle", 1993, 105, "Comedy"
	);
	
	Movie mail = new Movie (
			"tt0128853", "You've got mail", 1998, 119, "Comedy"
	);
	
	Role hanksSleepless = new Role (
		hanks.getPersonID(), sleepless.getMovieID(), 
		"Samuel 'Sam' Baldwin", "actor", 1
	);

	Role hanksMail = new Role (
		hanks.getPersonID(), mail.getMovieID(), 
		"Joe Fox", "actor", 1
	);
	
	Role ryanSleepless = new Role (
		ryan.getPersonID(), sleepless.getMovieID(),
		"Annie Reed", "actress", 2
	);
	
	Role ryanMail = new Role (
		ryan.getPersonID(), mail.getMovieID(),
		"Kathleen Kelly", "actress", 2
	);
	
	private Database ()
	{			
	}
	
	public static Database getInstance () {
		if (instance == null) {
			instance = new Database ();
		}
		
		return instance;
	}
	
	/**
	 * Finds the actors that match the given conditions.  Only those 
	 * parameters that have non-empty values are included in the query.  
	 * Those parameters that are included are combined using AND.
	 * @param firstName the first name of the actor(s) to locate
	 * @param lastName the last name of the actor(s) to locate
	 * @param birthYear the year when the actor(s) to locate were born
	 * @param deathYear the year when the actor(s) to locate died
	 * @return a List of Actor objects representing the actor(s) matching the
	 * specified conditions
	 * @throws SQLException if an exception occurs connecting to the DB,
	 * or in processing the results 
	 */
	public List<Actor> findActors (
		String firstName, String lastName,
		int birthYear, int deathYear
	) throws SQLException
	{		
		ArrayList<Actor> actors = new ArrayList<Actor> ();
		actors.add (hanks);
		actors.add (ryan);
		return actors;		
	}
	
	/**
	 * Finds the movies that match the given conditions.  All conditions
	 * are combined using AND.
	 * @param movieTitle the title of the movie(s) to be located, or an
	 * empty string if title should not be included as part of the criteria
	 * @param year the year the movie was made, or -1 if year should not be
	 * included as part of the criteria
	 * @param runningTimeComparator a string indicating the type of comparison
	 * to be used for the running time.  This string can be used directly in
	 * the SQL query
	 * @param runningTimeValue the running time of the movie(s) to be located,
	 * or -1 if running time should not be included as part of the criteria
	 * @param primaryGenre the primary genre of the movie(s) to be located,
	 * or an empty string if the primary genre should not be included as part
	 * of the criteria
	 * @param director the director of the movie(s) to be located, or an empty string
	 * if the director should not be included as part of the criteria
	 * @return a List of Movie objects representing the movie(s) matching the
	 * specified conditions
	 * @throws SQLException if an exception occurs connecting to the DB,
	 * or in processing the results
	 */
	public List<Movie> findMovies (
		String movieTitle, int year, String runningTimeComparator, 
		int runningTimeValue, String primaryGenre, String director
	) throws SQLException {
		ArrayList<Movie> movies = new ArrayList<Movie> ();
		movies.add (sleepless);
		movies.add (mail);
		return movies;
	}
	
	/**
	 * Finds all the roles played by an actor.
	 * @param actor the actor to search for
	 * @return a List of Role objects representing the roles played by the
	 * specified actor
	 * @throws SQLException if an exception occurs connecting to the DB,
	 * or in processing the results
	 */
	public List<Role> findMoviesForActor (Actor actor) throws SQLException {
		ArrayList<Role> roles = new ArrayList<Role> ();
		
		if (actor.getPersonID().equals(hanks.getPersonID())) {
			roles.add (hanksSleepless);
			roles.add (hanksMail);
		}
		
		if (actor.getPersonID().equals(ryan.getPersonID())) {
			roles.add (ryanSleepless);
			roles.add (ryanMail);
		}				
		return roles;
	}
	
	/**
	 * Finds the cast of a given movie
	 * @param movie the movie to search for
	 * @return a List of Role objects representing the roles in the
	 * specified movie
	 * @throws SQLException if an exception occurs connecting to the DB,
	 * or in processing the results
	 */
	public List<Role> findRolesInMovie (Movie movie) throws SQLException {
		ArrayList<Role> roles = new ArrayList<Role> ();
		if (movie.getMovieID() == sleepless.getMovieID()) {
			roles.add (hanksSleepless);
			roles.add (ryanSleepless);
		}
		
		if (movie.getMovieID() == mail.getMovieID()) {
			roles.add (hanksMail);
			roles.add (ryanMail);
		}
		
		return roles;
	}
	
	/**
	 * Finds a given actor based on Person ID
	 * @param personID the ID to search for
	 * @return the actor with the given ID, or null if none can be found
	 * @throws SQLException if an exception occurs connecting to the DB,
	 * or in processing the results
	 */
	public Actor findActorByID (String personID) throws SQLException {
		if (personID.equals(hanks.getPersonID())) {
			return hanks;
		}
		
		if (personID.equals(ryan.getPersonID())) {
			return ryan;
		}
		
		return null;
	}
	
	/**
	 * Finds a given movie based on Title ID
	 * @param titleID the ID to search for
	 * @return the movie with the given ID, or null if none can be found
	 * @throws SQLException if an exception occurs connecting to the DB,
	 * or in processing the results
	 */
	public Movie findMovieByID (String titleID) throws SQLException {
		if (titleID.equals(sleepless.getMovieID())) {
			return sleepless;
		}
		
		if (titleID.equals(mail.getMovieID())) {
			return mail;
		}
		
		return null;
	}
	
	/** Retrieves the list of possible genres from the database
	 * 
	 * @return a list of strings representing the possible genres
	 * @throws SQLException if an exception occurs connecting to the DB,
	 * or in processing the results
	 */
	public List<String> findGenres () throws SQLException {
		ArrayList<String> genres = new ArrayList<String> ();
		genres.add("Comedy");
		genres.add("Drama");
		genres.add("Romance");
		genres.add("War");
		
		return genres;
	}
}
